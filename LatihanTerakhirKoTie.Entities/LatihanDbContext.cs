﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace LatihanTerakhirKoTie.Entities
{
    public partial class LatihanDbContext : DbContext
    {
        public virtual DbSet<Worker> Worker { get; set; }

        public LatihanDbContext(DbContextOptions<LatihanDbContext> dbContextOptions) : base(dbContextOptions)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Worker>(entity =>
            {
                entity.Property(e => e.WorkerId).ValueGeneratedNever();

                entity.Property(e => e.Name).IsUnicode(false);
            });
        }
    }
}
