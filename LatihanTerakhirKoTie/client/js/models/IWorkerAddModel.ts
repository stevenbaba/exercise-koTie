﻿export interface IWorkerAddModel {
    name: string;

    weight: number;

    gender: boolean;
}