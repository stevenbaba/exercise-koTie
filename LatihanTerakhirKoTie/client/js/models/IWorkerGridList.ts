﻿import { IWorkerList } from './IWorkerList';

export interface IWorkerGridList {
    workers: IWorkerList[];
}