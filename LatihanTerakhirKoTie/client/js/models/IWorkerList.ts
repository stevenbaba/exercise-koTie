﻿
export interface IWorkerList {
    workerId: string;

    name: string;
    
    weight: number;

    gender: boolean;
}