﻿
export interface IWorkerListFilter {
    name: string;

    gender: string;

    order: string;

    iDesc: boolean;

}