﻿import Axios from 'axios';
import Noty from 'noty';
import { IWorkerGridList } from '../models/IWorkerGridList';
import { IWorkerListFilter } from '../models/IWorkerListFilter';
import { IWorkerAddModel } from '../models/IWorkerAddModel';

export class WorkerService {

    workerGrid: IWorkerGridList = {
        workers: []
    };

    addWorker: IWorkerAddModel = {
        gender: false,
        name: '',
        weight: 1
    };

    orderByOption = ['Worker Name', 'Weight'];

    workerFilter: IWorkerListFilter = {
        gender: '',
        iDesc: false,
        name: '',
        order: 'WorkerName'
    };


    async getProductGrid() {
        let response = await Axios.get<IWorkerGridList>('/api/v1/worker', {
            params: this.workerFilter
        });

        this.workerGrid = response.data;
    }

    async createWorker() {
        try {
            let response = await Axios.post('/api/v1/worker', this.addWorker);

            new Noty({
                text: response.data,
                type: 'success',
                theme: 'relax',
                timeout: 1000
            }).show();

        } catch {
            new Noty({
                text: 'System Error'
            }).show;
        } finally {
            this.resetCreateField();
            await this.getProductGrid();
        }
    }

    resetCreateField() {
        this.addWorker = {
            name: '',
            gender: false,
            weight: 1
        };
    }

}

export let WorkerListServicesSingleton = new WorkerService();