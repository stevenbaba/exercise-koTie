﻿import Vue from 'vue'
import Component from 'vue-class-component';
import { render, staticRenderFns } from './WorkerList.vue.html'
import { WorkerListServicesSingleton } from '../../services/WorkerServices';
import { IWorkerListFilter } from '../../models/IWorkerListFilter'
import { IWorkerAddModel } from '../../models/IWorkerAddModel';


@Component({
    render, staticRenderFns,
    created: async function (this: WorkerList) {
        await this.workerListState.getProductGrid();
    }
})

export class WorkerList extends Vue {
    workerListState = WorkerListServicesSingleton;

    genderOptions = ['L', 'P'];

    orderOptions = ["Worker Name", "Weight"];

    filter: IWorkerListFilter = {
        gender: 'P',
        name: '',
        order: 'Worker Name',
        iDesc: false
    };

    addWorker: IWorkerAddModel = {
        gender: false,
        name: '',
        weight: 1
    };

    async search() {
        this.workerListState.workerFilter = this.filter;
        await this.workerListState.getProductGrid();
    }

    get getWorkerGrid() {
        return this.workerListState.workerGrid;
    }

    async create() {
        this.workerListState.addWorker = this.addWorker;
        await this.workerListState.createWorker();
    }
}