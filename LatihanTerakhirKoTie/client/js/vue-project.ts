import Vue from 'vue';
import { Hello } from './components/Hello';
import { WorkerList } from './components/workers/WorkerList';

// components must be registered BEFORE the app root declaration
Vue.component('hello', Hello);
Vue.component('worker-list', WorkerList);

// bootstrap the Vue app from the root element <div id="app"></div>
new Vue().$mount('#app');
