﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LatihanTerakhirKoTie.Entities;
using LatihanTerakhirKoTie.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LatihanTerakhirKoTie.API
{
    [Produces("application/json")]
    [Route("api/v1/Worker")]
    public class WorkerApiController : Controller
    {
        private readonly LatihanDbContext DB;

        public WorkerApiController(LatihanDbContext latihanDbContext)
        {
            this.DB = latihanDbContext;
        }

        // GET: api/WorkerApi
        [HttpGet]
        public async Task<IActionResult> Get(WorkerListFilterModel value)
        {
            var workerQueriable = this.DB.Worker.AsQueryable();

            if (string.IsNullOrEmpty(value.Name) == false)
            {
                workerQueriable = workerQueriable.Where(Q => Q.Name.Contains(value.Name) == true);
            }
            if (string.IsNullOrEmpty(value.Gender) == false)
            {
                //defaultnya perempuan
                var genderSearch = false;
                if (value.Gender == "L")
                {
                    //jika laki"
                    genderSearch = true;
                }

                workerQueriable = workerQueriable.Where(Q => Q.Gender == genderSearch);
            }
            if (value.Order == "Worker Name")
            {
                if (value.iDesc == true)
                {
                    workerQueriable = workerQueriable.OrderByDescending(Q => Q.Name);
                }
                else
                {
                    workerQueriable = workerQueriable.OrderBy(Q => Q.Name);
                }
            }
            else
            {
                if (value.iDesc == true)
                {
                    workerQueriable = workerQueriable.OrderByDescending(Q => Q.Weight);
                }
                else
                {
                    workerQueriable = workerQueriable.OrderBy(Q => Q.Weight);
                }
            }

            var workers = await workerQueriable.Select(Q => new WorkerListModel
            {
                Gender = Q.Gender,
                Name = Q.Name,
                Weight = (int)Q.Weight,
                WorkerId = Q.WorkerId
            }).ToListAsync();

            var workerGrid = new WorkerGridListModel
            {
                workers = workers
            };

            return Ok(workerGrid);

        }

        // GET: api/WorkerApi/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/WorkerApi
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AddWorkerModel value)
        {
            if(ModelState.IsValid == false)
            {
                return BadRequest("data tidak valid");
            }

            this.DB.Worker.Add(new Entities.Worker
            {
                Gender = value.gender,
                Name = value.Name,
                Weight = value.Weight,
                WorkerId = Guid.NewGuid()
            });

            await this.DB.SaveChangesAsync();

            return Ok("Data sudah Masuk"); 
        }

        // PUT: api/WorkerApi/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
