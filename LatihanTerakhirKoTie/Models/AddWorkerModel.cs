﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LatihanTerakhirKoTie.Models
{
    public class AddWorkerModel
    {
        [Required]
        [StringLength(255,MinimumLength =8)]
        public string Name { set; get; }

        [Required]
        [Range(30,100)]
        public int Weight { set; get; }

        [Required]
        public bool gender { set; get; }
    }

}

