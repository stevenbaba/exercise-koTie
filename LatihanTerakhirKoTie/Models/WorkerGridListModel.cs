﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LatihanTerakhirKoTie.Models
{
    public class WorkerGridListModel
    {
        public List<WorkerListModel> workers { set; get; }
    }
}
