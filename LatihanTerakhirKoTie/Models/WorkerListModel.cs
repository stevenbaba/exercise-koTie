﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LatihanTerakhirKoTie.Models
{
    public class WorkerListModel
    {
        public Guid WorkerId { set; get; }

        public string Name { set; get; }

        public int Weight { set; get; }

        public bool Gender { set; get; }
    }
}
