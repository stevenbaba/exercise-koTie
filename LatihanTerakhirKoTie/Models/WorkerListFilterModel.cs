﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LatihanTerakhirKoTie.Models
{
    public class WorkerListFilterModel
    {
        public string Name { set; get; }

        public string Gender { set; get; }

        public string Order { get; set; }

        public bool iDesc { set; get; }

    }
}
